<?php

/**
 * Represents a user-defined form.
 *
 * @author Yaron Koren
 * @file
 * @ingroup PF
 */
class PFForm {
	private $mFormName;
	private $mPageNameFormula = null;
	private $mCreateTitle = null;
	private $mEditTitle = null;
	private $mAssociatedCategory = null;
	private $mItems = [];

	public static function create( string $formName, array $items ) {
		$form = new PFForm();
		$form->mFormName = ucfirst( str_replace( '_', ' ', $formName ) );
		$form->mAssociatedCategory = null;
		$form->mItems = $items;
		return $form;
	}

	public function getFormName() {
		return $this->mFormName;
	}

	public function getItems() {
		return $this->mItems;
	}

	public function setPageNameFormula( ?string $pageNameFormula ) {
		if ( !empty( $pageNameFormula ) ) {
			$this->mPageNameFormula = $pageNameFormula;
		}
	}

	public function setCreateTitle( ?string $createTitle ) {
		if ( !empty( $createTitle ) ) {
			$this->mCreateTitle = $createTitle;
		}
	}

	public function setEditTitle( ?string $editTitle ) {
		if ( !empty( $editTitle ) ) {
			$this->mEditTitle = $editTitle;
		}
	}

	public function setAssociatedCategory( ?string $associatedCategory ) {
		if ( !empty( $associatedCategory ) ) {
			$this->mAssociatedCategory = $associatedCategory;
		}
	}

	public function createMarkup( $includeFreeText = true, $freeTextLabel = null ) {
		$title = Title::makeTitle( PF_NS_FORM, $this->mFormName );
		$fs = PFUtils::getSpecialPage( 'FormStart' );
		$form_start_url = PFUtils::titleURLString( $fs->getPageTitle() ) . "/" . $title->getPartialURL();
		$form_description = wfMessage( 'pf_form_docu', $this->mFormName, $form_start_url )->inContentLanguage()->text();
		$form_input = "{{#forminput:form=" . str_replace( ',', '\,', $this->mFormName );
		if ( $this->mAssociatedCategory ) {
			$form_input .= "|autocomplete on category=" . $this->mAssociatedCategory;
		}
		$form_input .= "}}\n";
		$text = <<<END
<noinclude>
$form_description

$form_input
</noinclude><includeonly>

END;
		if ( $this->mPageNameFormula || $this->mCreateTitle || $this->mEditTitle ) {
			$text .= "{{{info";
			if ( $this->mPageNameFormula ) {
				$text .= "|page name=" . $this->mPageNameFormula;
			}
			if ( $this->mCreateTitle ) {
				$text .= "|create title=" . $this->mCreateTitle;
			}
			if ( $this->mEditTitle ) {
				$text .= "|edit title=" . $this->mEditTitle;
			}
			$text .= "}}}\n";
		}
		$text .= Html::element( "div", [
			"id" => "wikiPreview",
			"style" => "display: none; padding-bottom: 25px; margin-bottom: 25px; "
			. "border-bottom: 1px solid #AAAAAA;"
		] ) . "\n";

		foreach ( $this->mItems as $item ) {
			if ( $item['type'] == 'template' ) {
				$template = $item['item'];
				$text .= $template->createMarkup() . "\n";
			} elseif ( $item['type'] == 'section' ) {
				$section = $item['item'];
				$text .= $section->createMarkup() . "\n";
			}
		}

		if ( $includeFreeText ) {
			if ( $freeTextLabel === null ) {
				$freeTextLabel = wfMessage( 'pf_form_freetextlabel' )->inContentLanguage()->text();
			}
			$text .= <<<END
'''$freeTextLabel:'''

{{{standard input|free text|rows=10}}}

END;
		}
		$text .= "</includeonly>\n";

		return $text;
	}

}
