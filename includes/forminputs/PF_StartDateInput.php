<?php
/**
 * @file
 * @ingroup PF
 */

/**
 * @ingroup PFFormInput
 */
class PFStartDateInput extends PFDateInput {

	public static function getName(): string {
		return 'start date';
	}

	public static function getDefaultPropTypes(): array {
		return [];
	}

	public static function getOtherPropTypesHandled(): array {
		return [ '_dat' ];
	}

	public static function getDefaultCargoTypes(): array {
		return [
			'Start date' => [],
		];
	}

	public function getInputClass(): string {
		return 'dateInput startDateInput';
	}
}
