<?php
/**
 * @file
 * @ingroup PF
 */

/**
 * @ingroup PFFormInput
 */
class PFStartDateTimeInput extends PFDateTimeInput {

	public static function getName(): string {
		return 'start datetime';
	}

	public function getInputClass(): string {
		return 'dateTimeInput startDateTimeInput';
	}

	public static function getDefaultCargoTypes(): array {
		return [
			'Start datetime' => [],
		];
	}
}
