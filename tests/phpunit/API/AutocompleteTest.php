<?php
/**
 * Copyright (C) 2021  NicheWork, LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Mark A. Hershberger  <mah@nichework.com>
 */
namespace MediaWiki\Extension\PageReForms\Tests\PHPUnit\API;

use ApiTestCase;

/**
 * @covers MediaWiki\Extension\PageReForms\Autocomplete\API
 * @group PageReForms
 * @group medium
 *
 * Note the @group annotation is necessary for api tests.  Can be medium or large.
 */
class AutocompleteTest extends ApiTestCase {

	public function setUp(): void {
		parent::setUp();
	}

	/**
	 * @dataProvider propertyDataProvider
	 */
	public function testProperty( $args, $expected ) {
		$args['action'] = 'pfautocomplete';
		$args['format'] = 'json';
		list( $result, $request, $session ) = $this->doApiRequest( $args );
		$this->assertTrue( isset( $result['pfautocomplete'] ), "pfautocomplete result exists" );
		$this->assertSame( $expected, $result['pfautocomplete'], "pfautocomplete got expected result" );
	}

	/**
	 * Get your data here
	 */
	public function propertyDataProvider() {
		return [
			[['property' => 'Has bogus thing',
			  'baseprop' => 'Has bogus type',
			  'basevalue' => 'bogus base'],
			 []],
			[['category' => 'Completions',
			  'substr' => 'none'],
			 []]

		];
	}
}
