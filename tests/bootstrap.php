<?php

if ( PHP_SAPI !== 'cli' && PHP_SAPI !== 'phpdbg' ) {
	die( 'Not an entry point' );
}

error_reporting( E_ALL | E_STRICT );
date_default_timezone_set( 'UTC' );
ini_set( 'display_errors', 1 );

$autoloaderClassPath = getenv("MW_INSTALL_PATH") . '/extensions/SemanticMediaWiki/tests/autoloader.php';
if ( !is_readable( $autoloaderClassPath ) ) {
	die( "\nThe Semantic MediaWiki test autoloader is not available\n" );
}

if ( !defined( 'PF_VERSION' ) ) {
	die( "\nPageReForms is not available, please check your Composer or LocalSettings.\n" );
}

print sprintf( "\n%-20s%s\n", "PageReForms: ", PF_VERSION );

$autoloader = require $autoloaderClassPath;
$autoloader->addPsr4( 'SMW\\ApprovedRevs\\Tests\\', __DIR__ . '/phpunit/Unit' );
$autoloader->addPsr4( 'SMW\\ApprovedRevs\\Tests\\Integration\\', __DIR__ . '/phpunit/Integration' );
unset( $autoloader );
