<?php

$cfg = require __DIR__ . '/../vendor/mediawiki/mediawiki-phan-config/src/config.php';

# Files that would otherwise be ignored:
$cfg['file_list'] = array_merge(
	$cfg['file_list'] ?? [],
	[
		# Typically top-level .php files
	]
);

# Relative paths to other extensions that this one depends on:
$otherExtensions = [
	__DIR__ . '/../../AdminLinks',
	__DIR__ . '/../../Cargo',
	__DIR__ . '/../../PageSchemas',
	__DIR__ . '/../../SemanticMediaWiki',
];

$dirs = [ 'includes', 'specials', 'src' ];

$cfg['directory_list'] = array_merge(
	$cfg['directory_list'] ?? [], $otherExtensions, $dirs
);

$cfg['exclude_analysis_directory_list'] = array_merge(
	$cfg['exclude_analysis_directory_list'] ?? [], $otherExtensions
);

# Put messages you want to suppress here.
$cfg['suppress_issue_types'] = array_merge(
	$cfg['suppress_issue_types'] ?? [], [
		'PhanCompatibleNullableTypePHP70',
		'PhanCompatibleVoidTypePHP70',
		'PhanSuspiciousValueComparison'
	]
);

return $cfg;
