<?php
/**
 * Copyright (C) 2021  NicheWork, LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Mark A. Hershberger  <mah@nichework.com>
 */

namespace MediaWiki\Extension\PageReForms;

use GlobalVarConfig;

class Config extends GlobalVarConfig {

	/** @var self */
	protected static $instance;

	public const _24_HOUR_TIME = '24HourTime';
	public const AUTOCOMPLETE_CACHE_TIMEOUT = 'AutocompleteCacheTimeout';
	public const AUTOCOMPLETE_ON_ALL_CHARS = 'AutocompleteOnAllChars';
	public const AUTOCOMPLETE_VALUES = 'AutocompleteValues';
	public const AUTOEDIT_NAMESPACES = 'AutoeditNamespaces';
	public const CACHE_AUTOCOMPLETE_VALUES = 'CacheAutocompleteValues';
	public const CACHE_FORM_DEFINITIONS = 'CacheFormDefinitions';
	public const CALENDAR_PARAMS = 'CalendarParams';
	public const CALENDAR_VALUES = 'CalendarValues';
	public const CARGO_FIELDS = 'CargoFields';
	public const CHECKBOXES_SELECT_ALL_MINIMUM = 'CheckboxesSelectAllMinimum';
	public const CONT_LANG_MONTHS = 'ContLangMonths';
	public const CONT_LANG_NO = 'ContLangNo';
	public const CONT_LANG_YES = 'ContLangYes';
	public const DEPENDENT_FIELDS = 'DependentFields';
	public const DISABLE_OUTSIDE_SERVICES = 'DisableOutsideServices';
	public const EMBEDDED_TEMPLATES = 'EmbeddedTemplates';
	public const FIELD_PROPERTIES = 'FieldProperties';
	public const FORM_CACHE_TYPE = 'FormCacheType';
	public const GOOGLE_MAPS_KEY = 'GoogleMapsKey';
	public const GRID_PARAMS = 'GridParams';
	public const GRID_VALUES = 'GridValues';
	public const HEIGHT_FOR_MINIMIZING_INSTANCES = 'HeightForMinimizingInstances';
	public const IGNORE_TITLE_PATTERN = 'IgnoreTitlePattern';
	public const LINK_ALL_RED_LINKS_TO_FORMS = 'LinkAllRedLinksToForms';
	public const LIST_SEPARATOR = 'ListSeparator';
	public const MAPS_WITH_FEEDERS = 'MapsWithFeeders';
	public const MAX_AUTOCOMPLETE_VALUES = 'MaxAutocompleteValues';
	public const MAX_LOCAL_AUTOCOMPLETE_VALUES = 'MaxLocalAutocompleteValues';
	public const RENAME_EDIT_TABS = 'RenameEditTabs';
	public const RENAME_MAIN_EDIT_TAB = 'RenameMainEditTab';
	public const RUN_QUERY_FORM_AT_TOP = 'RunQueryFormAtTop';
	public const SHOW_EXPAND_ALL_LINK = 'ShowExpandAllLink';
	public const SHOW_ON_SELECT = 'ShowOnSelect';
	public const SHOW_TABS_FOR_ALL_HELPER_FORMS = 'ShowTabsForAllHelperForms';
	public const SIMPLE_UPLOAD = 'SimpleUpload';
	public const USE_DISPLAY_TITLE = 'UseDisplayTitle';
	public const VISUAL_EDITOR_MAX_HEIGHT = 'VisualEditorMaxHeight';

	public function __construct() {
		parent::__construct( 'wgPageForms' );
	}

	/**
	 * Factory method for MediaWikiServices
	 * @return Config
	 */
	public static function getInstance() {
		if ( !self::$instance ) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}
