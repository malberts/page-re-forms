<?php
/**
 * @file
 * @ingroup PF
 */

/**
 * Adds and handles the 'pfautocomplete' action to the MediaWiki API.
 *
 * @ingroup PF
 *
 * @author Sergey Chernyshev
 * @author Yaron Koren
 */

namespace MediaWiki\Extension\PageReForms\Autocomplete;

use ApiBase;
use ApiMessage;

class API extends ApiBase {

	/** @var Autocomplete */
	protected $autocomplete;

	public function __construct( $query, $moduleName ) {
		parent::__construct( $query, $moduleName );
		$this->autocomplete = new Manager( $this->extractRequestParams() );
	}

	public function execute(): void {
		try {
			$formatted = $this->autocomplete->getCompletions();
		} catch ( Exception $e ) {
			$this->dieWithError( ApiMessage::create( $e->getMessage() ) );
		}

		// Set top-level elements.
		$result = $this->getResult();
		$result->setIndexedTagName( $formatted, 'p' );
		$result->addValue( null, $this->getModuleName(), $formatted );
	}

	protected function getAllowedParams() {
		return [
			'limit' => [
				ApiBase::PARAM_TYPE => 'limit',
				ApiBase::PARAM_DFLT => 10,
				ApiBase::PARAM_MIN => 1,
				ApiBase::PARAM_MAX => ApiBase::LIMIT_BIG1,
				ApiBase::PARAM_MAX2 => ApiBase::LIMIT_BIG2
			],
			'substr' => null,
			'property' => null,
			'category' => null,
			'concept' => null,
			'cargo_table' => null,
			'cargo_field' => null,
			'namespace' => null,
			'external_url' => null,
			'baseprop' => null,
			'base_cargo_table' => null,
			'base_cargo_field' => null,
			'basevalue' => null,
		];
	}

	protected function getParamDescription() {
		return [
			'substr' => 'Search substring',
			'property' => 'Semantic property for which to search values',
			'category' => 'Category for which to search values',
			'concept' => 'Concept for which to search values',
			'namespace' => 'Namespace for which to search values',
			'external_url' => 'Alias for external URL from which to get values',
			'baseprop' => 'A previous property in the form to check against',
			'basevalue' => 'The value to check for the previous property',
			// 'limit' => 'Limit how many entries to return',
		];
	}

	protected function getDescription() {
		return 'Autocompletion call used by the Page Forms extension (https://www.mediawiki.org/Extension:Page_Forms)';
	}

	protected function getExamples() {
		return [
			'api.php?action=pfautocomplete&substr=te',
			'api.php?action=pfautocomplete&substr=te&property=Has_author',
			'api.php?action=pfautocomplete&substr=te&category=Authors',
		];
	}
}
