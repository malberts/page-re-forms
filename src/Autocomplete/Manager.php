<?php
/**
 * @file
 * @ingroup PF
 */

/**
 * Adds and handles the 'pfautocomplete' action to the MediaWiki API.
 *
 * @ingroup PF
 *
 * @author Sergey Chernyshev
 * @author Yaron Koren
 */

namespace MediaWiki\Extension\PageReForms\Autocomplete;

use CargoSQLQuery;
use Exception;
use MediaWiki\Extension\PageReForms\Config;
use PFFormUtils;
use PFValuesUtils;
use SMW\ApplicationFactory;
use SMW\DataValueFactory;
use SMW\Query\Language\ThingDescription;
use SMW\StringCondition;
use SMWQuery as Query;

class Manager {

	/** @var ?string */
	protected $basePropertyName;
	/** @var ?string */
	protected $baseCargoTable;
	/** @var ?string */
	protected $baseCargoField;
	/** @var ?string */
	protected $substr;
	/** @var ?string */
	protected $nameSpace;
	/** @var ?string */
	protected $propertyParam;
	/** @var ?\SMW\DIProperty */
	protected $property;
	/** @var ?string */
	protected $category;
	/** @var ?string */
	protected $concept;
	/** @var ?string */
	protected $cargoTable;
	/** @var ?string */
	protected $cargoField;
	/** @var ?string */
	protected $externalUrl;
	/** @var ?string */
	protected $baseValue;
	/** @var Config */
	protected $conf;
	/** @var bool */
	protected $useDisplayTitle;
	/** @var int */
	protected $maxAutocompleteValues;
	/** @var bool */
	protected $cacheAutocompleteValues;
	/** @var int */
	protected $autocompleteCacheTimeout;
	/** @var ?string */
	protected $cacheKey;
	/** @var ?object */
	protected $cache;

	public function __construct( $params ) {
		$this->substr = $params['substr'] ?? null;
		$this->nameSpace = $params['namespace'] ?? null;
		$this->propertyParam = $params['property'] ?? null;
		$this->category = $params['category'] ?? null;
		$this->concept = $params['concept'] ?? null;
		$this->cargoTable = $params['cargo_table'] ?? null;
		$this->cargoField = $params['cargo_field'] ?? null;
		$this->externalUrl = $params['external_url'] ?? null;
		$this->basePropertyName = $params['baseprop'] ?? null;
		$this->baseCargoTable = $params['base_cargo_table'] ?? null;
		$this->baseCargoField = $params['base_cargo_field'] ?? null;
		$this->baseValue = $params['baseValue'] ?? null;
		// $limit = $params['limit'] ?? null;

		$this->conf = Config::getInstance();
		$this->useDisplayTitle = $this->conf->get( Config::USE_DISPLAY_TITLE );
		$this->maxAutocompleteValues = intval( $this->conf->get( Config::MAX_AUTOCOMPLETE_VALUES ) );
		$this->cacheAutocompleteValues = $this->conf->get( Config::CACHE_AUTOCOMPLETE_VALUES );
		$this->autocompleteCacheTimeout = intval( $this->conf->get( Config::AUTOCOMPLETE_CACHE_TIMEOUT ) );

		// Set up cache if allowed
		if ( $this->cacheAutocompleteValues ) {
			$this->cache = $this->getCache();
			$this->cacheKey = $this->getCacheKey();
		}
	}

	private function getCache() {
		return PFFormUtils::getFormCache();
	}

	protected function formatCompletions( $data ): array {
		// Format data as the API requires it - this is not needed
		// for "values from url", where the data is already formatted
		// correctly.
		$formattedData = $data;
		if ( $this->externalUrl === null ) {
			$formattedData = [];
			foreach ( $data as $index => $value ) {
				if ( $this->useDisplayTitle ) {
					$formattedData[] = [ 'title' => $index, 'displaytitle' => $value ];
				} else {
					$formattedData[] = [ 'title' => $value ];
				}
			}
		}

		return $formattedData;
	}

	protected function getCachedCompletion() {
		// @TODO
		return null;
	}

	public function getCompletions() {
		if (
			$this->basePropertyName === null
			&& $this->baseCargoTable === null
			&& strlen( $this->substr ) === 0
		) {
			throw new Exception( [ 'apierror-missingparam', 'substr' ], 'param_substr' );
		}

		$data = $this->getCachedCompletion();
		if ( $data !== null ) {
			return $data;
		}

		if ( $this->basePropertyName !== null && $this->propertyParam !== null ) {
			$data = $this->getAllValuesForSubProperty();
		} elseif ( $this->propertyParam !== null ) {
			$data = $this->getPropertyValueCompletions();
		} elseif ( $this->category !== null ) {
			$data = PFValuesUtils::getAllPagesForCategory( $this->category, 3, $this->substr );
			if ( $this->useDisplayTitle ) {
				$data = PFValuesUtils::disambiguateLabels( $data );
			}
		} elseif ( $this->concept !== null ) {
			$data = PFValuesUtils::getAllPagesForConcept( $this->concept, $this->substr );
			if ( $this->useDisplayTitls ) {
				$data = PFValuesUtils::disambiguateLabels( $data );
			}
		} elseif ( $this->cargoTable !== null && $this->cargoField !== null ) {
			$data = $this->getAllValuesForCargoField();
		} elseif ( $this->nameSpace !== null ) {
			$data = PFValuesUtils::getAllPagesForNamespace( $this->nameSpace, $this->substr );
		} elseif ( $this->externalUrl !== null ) {
			$data = PFValuesUtils::getValuesFromExternalURL( $this->externalUrl, $this->substr );
		} else {
			$data = [];
		}

		return $this->formatCompletions( $data );
	}

	private function setupSMW() {
		if ( !class_exists( DataValueFactory::class ) ) {
			throw new Exception(
				'Semantic MediaWiki must be installed to query on "property"', 'param_property'
			);
		}
		$this->appFactory = ApplicationFactory::getInstance();
		$this->property = $this->appFactory->getDataItemFactory()->newDIProperty( $this->propertyParam );
		$this->requestOptions = $this->appFactory->getQueryFactory()->newRequestOptions();
		$this->requestOptions->sort = true;
		$this->requestOptions->setLimit( $this->maxAutocompleteValues );
	}

	protected function stringifyResults( array $results ) {
		$data = [];
		foreach ( $results as $item ) {
			if ( method_exists( $item, 'getString' ) ) {
				$data[] = $item->getString();
			} else if ( method_exists( $item, 'getDBkey' ) ) {
				$data[] = $item->getDBkey();
			} else {
				throw new Exception(
					"Need a stringify method for "
					. SMWDataItem::getDataItemClassNameForId( $item->getDIType() )
				);
			}
		}
		return $data;
	}

	/**
	 * Construct a query to get property values.  This is equivalent the following ask query:
	 *
	 * {{#ask:
	 *   [[$property::+]] [[$basePropertyName::$baseName]]
	 *   |?$property
	 * }}
	 *
	 * @return array
	 */
	private function getAllValuesForSubProperty(): array {
		$this->setupSMW();

		$descFactory = $this->appFactory->getQueryFactory()->newDescriptionFactory();
		$store = $this->appFactory->getStore();

		// equivalent to "[[property::+]]" in an Ask query
		$propertyDescription = $descFactory->newSomeProperty( $this->property, new ThingDescription() );

		// equivalent to "[[baseProperty::baseValue]]"
		$baseProperty = $this->appFactory->getDataItemFactory()->newDIProperty( $this->basePropertyName );
		$baseDV = $this->appFactory
				->getDataValueFactory()->newDataValueByProperty( $baseProperty, $this->baseValue );
		$baseDescriptionDV = $descFactory->newFromDataValue( $baseDV );

		// Construct the whole query description by using AND:
		//     [[baseProperty::baseValue]] [[property::+]]
		$description = $descFactory->newConjunction( [ $baseDescriptionDV, $propertyDescription ] );
		$query = new Query( $description );
		$queryResult = $store->getQueryResult( $query );

		// $queryResult->getResults() gives us an array of WikiPage data items
		$data = [];
		foreach ( $queryResult->getResults() as $item ) {
			array_map( function ( $datum ) use ( &$data ) {
				$data[] = $datum;
			},
				/**
				 * Here we do the printout selection (i.e ?property) for each result,
				 * represented by item:
				 */
				$store->getPropertyValues( $item, $this->property, $this->requestOptions ) );
		}
		return $this->stringifyResults( $data );
	}

	/**
	 * This function takes a string and finds all values of a given property that start with it.
	 *
	 * @return array
	 */
	protected function getPropertyValueCompletions(): array {
		$this->setupSMW();
		$this->requestOptions->addStringCondition( $this->substr, StringCondition::STRCOND_PRE );

		return $this->stringifyResults(
			$this->appFactory->getPropertyValues( null, $this->property, $this->requestOptions )
		);
	}

	private function getAllValuesForCargoField(): array {
		if ( !class_exists( CargoSQLQuery::class ) ) {
			return [];
		}
		$maxAutocompleteValues = $this->conf->get( Config::MAX_AUTOCOMPLETE_VALUES );
		$cacheAutocompleteValues = $this->conf->get( Config::CACHE_AUTOCOMPLETE_VALUES );
		$autocompleteCacheTimeout = $this->conf->get( Config::AUTOCOMPLETE_CACHE_TIMEOUT );
		$autocompleteOnAllChars = $this->conf->get( Config::AUTOCOMPLETE_ON_ALL_CHARS );

		$values = [];
		$tablesStr = $this->cargoTable;
		$fieldsStr = $this->cargoField;
		$joinOnStr = '';
		$whereStr = '';

		// Use cache if allowed
		$cache = null;
		if ( $cacheAutocompleteValues ) {
			$cache = PFFormUtils::getFormCache();
			// Remove trailing whitespace to avoid unnecessary database selects
			$cacheKeyString = $this->cargoTable . '|' . $this->cargoField . '|' . rtrim( $substring );
			if ( $baseCargoTable !== null ) {
				$cacheKeyString .= '|' . $baseCargoTable . '|' . $baseCargoField . '|'
								. $this->baseValue;
			}
			$cacheKey = $cache->makeKey( 'pf-autocomplete', md5( $cacheKeyString ) );
			$values = $cache->get( $cacheKey );

			if ( is_array( $values ) && count( $values ) > 0 ) {
				// Return with results immediately
				return $values;
			} elseif ( !is_array( $values ) ) {
				$values = [];
			}
		}

		if ( $baseCargoTable !== null && $baseCargoField !== null ) {
			if ( $baseCargoTable !== $this->cargoTable ) {
				$tablesStr .= ", $baseCargoTable";
				$joinOnStr = "$this->cargoTable._pageName = $baseCargoTable._pageName";
			}
			$whereStr = "$baseCargoTable.$baseCargoField = \"{$this->baseValue}\"";
		}

		if ( $substring !== null ) {
			if ( $whereStr !== '' ) {
				$whereStr .= " AND ";
			}

			if ( $this->cargoFieldIsList() ) {
				// If it's a list field, we query directly on
				// the "helper table" for that field. We could
				// instead use "HOLDS LIKE", but this would
				// return false positives - other values that
				// have been listed alongside the values we're
				// looking for - at least for Cargo >= 2.6.
				$fieldTableName = $this->cargoTable . '__' . $this->cargoField;
				// Because of the way Cargo querying works, the
				// field table has to be listed first for only
				// the right values to show up.
				$tablesStr = $fieldTableName . ', ' . $tablesStr;
				if ( $joinOnStr !== '' ) {
					$joinOnStr = ', ' . $joinOnStr;
				}
				$joinOnStr = $fieldTableName . '._rowID=' .
					$this->cargoTable . '._ID' . $joinOnStr;

				$fieldsStr = $this->cargoField = '_value';
			}

			if ( $autocompleteOnAllChars ) {
				$whereStr .= "($this->cargoField LIKE \"%$substring%\")";
			} else {
				$whereStr .= "($this->cargoField LIKE \"$substring%\" "
						  . "OR $this->cargoField LIKE \"% $substring%\")";
			}
		}

		$sqlQuery = CargoSQLQuery::newFromValues(
			$tablesStr,
			$fieldsStr,
			$whereStr,
			$joinOnStr,
			$this->cargoField,
			$havingStr = null,
			$this->cargoField,
			$maxAutocompleteValues,
			$offsetStr = 0
		);
		$queryResults = $sqlQuery->run();

		if ( substr( $this->cargoField, 0, 1 ) !== '_' ) {
			$cargoFieldAlias = str_replace( '_', ' ', $this->cargoField );
		} else {
			$cargoFieldAlias = $this->cargoField;
		}

		foreach ( $queryResults as $row ) {
			// @TODO - this check should not be necessary.
			$value = $row->$cargoFieldAlias;
			if ( $value === '' ) {
				continue;
			}
			// Cargo HTML-encodes everything - let's decode double
			// quotes, at least.
			$values[] = str_replace( '&quot;', '"', $value );
		}

		if ( $cache && is_string( $cacheKey ) && count( $values ) > 0 ) {
			// Save to cache.
			$cache->set( $cacheKey, $values, $autocompleteCacheTimeout );
		}

		return $values;
	}

	protected function cargoFieldIsList() {
		// @TODO - this is duplicate work; the schema is retrieved
		// again when the CargoSQLQuery object is created. There should
		// be some way of avoiding that duplicate retrieval.
		try {
			$tableSchemas = CargoUtils::getTableSchemas( [ $this->cargoTable ] );
		} catch ( MWException $e ) {
			return false;
		}
		if ( !array_key_exists( $this->cargoTable, $tableSchemas ) ) {
			return false;
		}
		$tableSchema = $tableSchemas[$this->cargoTable];
		if ( !array_key_exists( $this->cargoField, $tableSchema->mFieldDescriptions ) ) {
			return false;
		}
		$fieldDesc = $tableSchema->mFieldDescriptions[$this->cargoField];
		return $fieldDesc->mIsList;
	}

}
