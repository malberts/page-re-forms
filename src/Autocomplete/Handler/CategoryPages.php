<?php

/**
 * Copyright (C) 2021  NicheWork, LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Mark A. Hershberger  <mah@nichework.com>
 */

namespace MediaWiki\Extension\PageReForms\Autocomplete\Handler;

#use MediaWiki\Extension\PageReForms\AutoComplete\Handler;
use IDatabase;
use PFValuesUtils;
use PFUtils;
use stdClass;
use Title;
use Wikimedia\Rdbms\ResultWrapper;

class CategoryPages {
	protected string $topCategory;
	protected array $categories;
	protected array $checkCategories;

	protected int $numLevels;
	protected ?string $substring;

	protected array $tables;
	protected array $columns;
	protected array $conditions;
	protected array $join = [];

	protected array $sortKeys = [];
	protected array $pages = [];
	protected array $newCategories = [];

	protected IDatabase $db;

	public function __construct( string $topCategory, int $numLevels, ?string $substring = null ) {
		$this->topCategory = str_replace( ' ', '_', $topCategory );
		$this->categories = [ $this->topCategory ];
		$this->checkCategories = [ $this->topCategory ];

		$this->numLevels = $numLevels;
		$this->substring = $substring;

		$this->tables = [ 'categorylinks', 'page' ];
		$this->columns = [ 'page_title', 'page_namespace' ];
		$this->conditions = [ 'cl_from = page_id' ];

		$this->db = wfGetDB( DB_REPLICA );
	}

	protected function handleDisplayTitle( $useDisplayTitle ) {
		if ( $useDisplayTitle ) {
			$this->tables['pp_displaytitle'] = 'page_props';
			$this->tables['pp_defaultsort'] = 'page_props';
			$this->columns['pp_displaytitle_value'] = 'pp_displaytitle.pp_value';
			$this->columns['pp_defaultsort_value'] = 'pp_defaultsort.pp_value';
			$this->join = [
				'pp_displaytitle' => [
					'LEFT JOIN', [
						'pp_displaytitle.pp_page = page_id',
						'pp_displaytitle.pp_propname = \'displaytitle\''
					]
				],
				'pp_defaultsort' => [
					'LEFT JOIN', [
						'pp_defaultsort.pp_page = page_id',
						'pp_defaultsort.pp_propname = \'defaultsort\''
					]
				]
			];
			if ( $this->substring != null ) {
				$this->conditions[] = '(pp_displaytitle.pp_value IS NULL AND (' .
								   PFValuesUtils::getSQLConditionForAutocompleteInColumn(
									   'page_title', $this->substring
								   ) . ')) OR ' .
								   PFValuesUtils::getSQLConditionForAutocompleteInColumn(
									   'pp_displaytitle.pp_value', $this->substring
								   ) . ' OR page_namespace = ' . NS_CATEGORY;
			}
		} else {
			if ( $this->substring != null ) {
				$this->conditions[] = PFValuesUtils::getSQLConditionForAutocompleteInColumn(
					'page_title', $this->substring
				) . ' OR page_namespace = ' . NS_CATEGORY;
			}
		}
	}

	protected function query( int $max ): ResultWrapper {
		// make the query
		return $this->db->select(
			$this->tables,
			$this->columns,
			$this->conditions,
			__METHOD__,
			[
				'ORDER BY' => 'cl_type, cl_sortkey',
				'LIMIT' => $max
			],
			$this->join
		);
	}

	protected function handleRow( stdClass $row ): void {
		$page_namespace = $row->page_namespace;
		$page_name = $row->page_title;
		var_dump($row);
		if ( $page_namespace === NS_CATEGORY ) {
			if ( !in_array( $page_name, $this->categories ) ) {
				$this->newCategories[] = $page_name;
			}
		} else {
			$cur_title = Title::makeTitleSafe( $page_namespace, $page_name );
			if ( $cur_title === null ) {
				// This can happen if it's a "phantom" page, in a namespace that no
				// longer exists.
				return;
			}
			$cur_value = PFUtils::titleString( $cur_title );
			if ( !in_array( $cur_value, $this->pages ) ) {
				if (
					property_exists( $row, 'pp_displaytitle_value' ) &&
					$row->pp_displaytitle_value !== null &&
					trim( str_replace(
						'&#160;', '', strip_tags( $row->pp_displaytitle_value )
					) ) !== ''
				) {
					$this->pages[ $cur_value . '@' ] = htmlspecialchars_decode(
						$row->pp_displaytitle_value
					);
				} else {
					$this->pages[ $cur_value . '@' ] = $cur_value;
				}
				if (
					property_exists( $row, 'pp_defaultsort_value' ) &&
					$row->pp_defaultsort_value !== null
				) {
					$this->sortKeys[ $cur_value ] = $row->pp_defaultsort_value;
				} else {
					$this->sortKeys[ $cur_value ] = $cur_value;
				}
			}
		}
	}

	/**
	 * Do the DB magic to get the pages
	 */
	protected function loadPages(): void {
		global $wgPageFormsMaxAutocompleteValues, $wgPageFormsUseDisplayTitle;

		for ( $level = $this->numLevels; $level > 0; $level-- ) {
			$this->newCategories = [];
			foreach ( $this->checkCategories as $category ) {
				$this->conditions['cl_to'] = $category;
				$this->handleDisplayTitle( $wgPageFormsUseDisplayTitle );
				foreach ( $this->query( $wgPageFormsMaxAutocompleteValues ) as $row ) {
					if ( !$row->page_title ) {
						continue;
					}
					$this->handleRow( $row );
				}
			}

			if ( count( $this->newCategories ) == 0 ) {
				return;
			} else {
				$this->categories = array_merge( $this->categories, $this->newCategories );
			}
			$this->checkCategories = array_diff( $this->newCategories, [] );
		}
	}


	/**
	 * array_multisort() unfortunately messes up array keys that are
	 * numeric - they get converted to 0, 1, etc. There are a few ways to
	 * get around this, but I (Yaron) couldn't get those working, so
	 * instead we're going with this hack, where all key values get
	 * appended with a '@' before sorting, which is then removed after
	 * sorting. It's inefficient, but it's probably good enough.
	 *
	 * @return string[] a sorted version of $this->pages, sorted via $this->sortKeys
	 */
	public function sorted(): array {
		if ( count( $this->pages ) === 0 ) {
			$this->loadPages();
		}

		array_multisort( $this->sortKeys, $this->pages );
		$newPages = [];

		foreach ( $this->pages as $key => $value ) {
			$fixedKey = rtrim( $key, '@' );
			$newPages[$fixedKey] = $value;
		}

		return $newPages;
	}
}
